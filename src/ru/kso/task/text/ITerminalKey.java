package ru.kso.task.text;

public interface ITerminalKey {

    String PREFIX = "-";
    String VERSION_KEY = PREFIX + "version";
    String ABOUT_KEY = PREFIX + "about";
    String HELP_KEY = PREFIX + "help";
}
