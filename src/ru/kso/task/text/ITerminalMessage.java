package ru.kso.task.text;

public interface ITerminalMessage extends ITerminalKey {

    String VERSION_MESSAGE = "Current program version is 1.0.0";
    String DEVELOPER_MAIL = "karasevsergey479@gmail.com";
    String ABOUT_MESSAGE = String.format("The developer is Karasev Sergey, e-mail: %s", DEVELOPER_MAIL);
    String HELP_KEY_INFO = String.format("For get a help enter command %s%n", HELP_KEY);
    String ABOUT_KEY_INFO = String.format("For get an information of the developer enter command %s%n", ABOUT_KEY);
    String VERSION_KEY_INFO = String.format("For get an information of the program version enter command %s", VERSION_KEY);
    String NULL_DATA_MESSAGE = String.format("Please, enter a command.%n%s", HELP_KEY_INFO);
    String HELP_MESSAGE = HELP_KEY_INFO + ABOUT_KEY_INFO + VERSION_KEY_INFO;
    String ERROR_MESSAGE = "Please, enter an one command, no more";
    String NOT_EXISTING_KEY_MESSAGE = "Entered command has not existing";
}
