package ru.kso.task;

import ru.kso.task.text.ITerminalMessage;

public class Application implements ITerminalMessage {

    public static void main(String[] args) {
        parseArgs(args);
    }

    private static void parseArgs(String[] args) {
        if (args.length == 0) {
            printMessage(NULL_DATA_MESSAGE);
            return;
        }
        if (args.length > 1) {
            printMessage(ERROR_MESSAGE);
            return;
        }
        String arg = args[0];
        executeCommand(arg);
    }

    private static void executeCommand(String arg) {
        switch (arg) {
            case HELP_KEY:
                printMessage(HELP_MESSAGE);
                break;
            case ABOUT_KEY:
                printMessage(ABOUT_MESSAGE);
                break;
            case VERSION_KEY:
                printMessage(VERSION_MESSAGE);
                break;
            default:
                printMessage(NOT_EXISTING_KEY_MESSAGE);
        }
    }

    private static void printMessage(String msg) {
        System.out.println(msg);
    }
}
